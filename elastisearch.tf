variable "domain" {
  default = "here-iot-es"
}

data "aws_subnet_ids" "selected" {
  vpc_id = "${aws_vpc.main.id}"
}

data "aws_subnet" "selected" {
   count = "${length(data.aws_subnet_ids.selected.ids)}"
   id    = "${tolist(data.aws_subnet_ids.selected.ids)[count.index]}" 
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_security_group" "elasticsearch" {
  name        = "${aws_vpc.main.id}-elasticsearch-${var.domain}"
  description = "Managed by Terraform"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "${aws_vpc.main.cidr_block}"
    ]
  }
}

resource "aws_iam_service_linked_role" "elasticsearch" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_elasticsearch_domain" "elasticsearch" {
  domain_name           = "${var.domain}"
  elasticsearch_version = "6.7"

  cluster_config {
    instance_type = "t2.small.elasticsearch"
  }

  vpc_options {
    subnet_ids = [ "${data.aws_subnet.selected[1].id}" ]
    security_group_ids = ["${aws_security_group.elasticsearch.id}"]
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain}/*"
        }
    ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  depends_on = [
    "aws_iam_service_linked_role.elasticsearch",
  ]
}
